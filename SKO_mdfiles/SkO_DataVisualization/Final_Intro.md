## Introduction

**Instructions**

You are now familiar with various data visualization tools such as Area Plots, Histograms, Bar Plots, including some specialized and advanced tools. It is now time to put this knowledge to the test. This final assignment is worth 40% of the entire grade for this course. 

**Review criteria**

This assignment will be graded by your peers who are also completing this course during the same session.


**Tasks to complete the peer reviewed questions**

1. Make the notebook shareable. **(2 marks)**

2. Use the pandas read_csv method to read the csv file into a pandas dataframe. Use the describe function to get the key statistics from the dataset. **(2 Marks)**

3. This question is in two parts. You are representing a startup that is looking to pilot carpool rideshare services in the USA. As the Data Scientist perform the following:

      Part 1: Create a box plot that compares carpooling habits between New York and California to determine where    to    focus your pilot efforts. **(4 Marks)**

      Part 2: From the above comparison create a bar plot to identify the top ten counties for the state that has a    higher number of people who currently carpool. These counties will represent your pilot geography. Note that you will  need to use the groupby function to group the mean carpool value for each county before you create the bar plot. **(4 Marks)**

4. Create a Choropleth map to plot the average income across states in the United States. **(3 Marks)**