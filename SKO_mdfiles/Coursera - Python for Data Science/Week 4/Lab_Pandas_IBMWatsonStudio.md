## LAB - Pandas with IBM Watson Studio

_If you have not created a Watson service before proceed with Step 1, otherwise go to Step 2:_

### Step 1: For New Users (with no Watson service):

For this project, you will use your IBM Watson Studio account from the previous chapter.  

Go to the IBM Cloud Watson Studio page:

[Click here](http://cocl.us/coursera_pandas_watson)

You will see the screen in the figure below. Click the icon in the red box:

![image](images/01_Watson_Studio_Landing_page_2.png)

Then click Watson, as shown below:

![image](images/02_Watson_Landing_page_menu_2.png)

Then click Browse Services.

![image](images/03_Watson_landing_page_2.png)

Scroll down and select **Watson Studio - Lite.**

![image](images/04_Browse_Service_3.png)

To create a Watson service using the Lite plan, click **Create.**

![image](images/05_Watson_Studio_Lite_sign_up_2.png)

Now click **Get Started.**

![image](images/05a_Watson_Studio2.png)

After creating the service continue with Step 2.

### Step 2: For Existing Users (who already have Watson Service):

Go to the IBM Cloud Dashboard and click **Services.**

![image](images/06_Dashboard_with_Service_2.png)

When you click on Services, all your existing services will be shown in the list. Click the **Watson Studio** service you created:

![image](images/07_Resource_List_2.png)

Then click **Get Started.**

![image](images/05a_Watson_Studio2.png)

### Step 3: Creating a Project

Now you have to Create a project.

Click on **Create a project:**

![image](images/08_01_Create_project.png)

On the Create a project page, click **Create an empty project**

![image](images/09_Create_project_5.png)

Provide a **Project Name** and **Description**, as shown below:

![image](images/10_New_Project_details_2a.png)

You must also create storage for the project.

Click **Add**

![image](images/11_New_Project_details_3.png)

On the Cloud Object Storage page, scroll down and then click **Create.**

![image](images/12_Select_storage_2.png)

In the Confirm Creation box, click **Confirm.**

![image](images/13_Confirm_Creation_2.png)

On the New project page, note that the storage has been added, and then click **Create.**

![image](14_Create_Project_Storage_Confirmed_3.png)

After creating the project continue with Step 3.

### Step 3: Adding a Notebook to the Project:

You need to add a Notebook to your project. Click **Add to project.**

![image](images/15_Add_to_Project_2.png)

In the list of asset types, click **Notebook:**

![image](images/16_Add_notebook_2.png)

On the New Notebook page, enter a name for the notebook, and then click **From URL.**
Copy this link:

https://cocl.us/PY0101EN43_EDX

Paste it into the **Notebook URL** box, and then click **Create Notebook.**

![image](images/17_Load_notebook_3.png)

You will see this Notebook:

![image](images/18_Notebook_loaded_2.png)




