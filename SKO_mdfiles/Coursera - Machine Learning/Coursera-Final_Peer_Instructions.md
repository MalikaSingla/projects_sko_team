### `Instructions`

Now that you have been equipped with the skills to use different Machine Learning algorithms, over the course of five weeks, you will have the opportunity to practice and apply it on a dataset. In this project, you will complete a notebook where you will build a classifier to predict whether a loan case will be paid off or not.

You load a historical dataset from previous loan applications, clean the data, and apply different classification algorithm on the data. You are expected to use the following algorithms to build your models:

- k-Nearest Neighbour

- Decision Tree

- Support Vector Machine

- Logistic Regression

The results is reported as the accuracy of each classifier, using the following metrics when these are applicable:

- Jaccard index

- F1-score

- LogLoass

### `Review criteria`

This final project will be graded by your peers who are completing this course during the same session. This project is worth **25 marks** of your total grade, broken down as follows:

- Building model using KNN, finding the best k and accuracy evaluation **(7 marks)**
 
- Building model using Decision Tree and accuracy evaluation **(6 marks)**
 
- Building model using SVM and accuracy evaluation **(6 marks)**
 
- Building model using Logistic Regression and accuracy evaluation **(6 marks)**

### `Step-By-Step Assignment Instructions`

#### Setup instructions:

**A- Create an account in Watson Studio if you dont have (If you already have it, jump to step B).**

1. Browse into https://www.ibm.com/cloud/watson-studio

2. Click on 'Start your free trial'

3. Enter your email, and click 'Next'

4. Enter your Name, and choose a Password. Then click on _Create Account_

5. Go to your email, and confirm your account.

6. Click on _Proceed_

7. In _Select Organization and Space_ form, leave everything as default, and click on _Continue_

8. It is done. Click on _Get started!_

**B- Sign in into Watson Studio and import your notebook**

1. Sign in into https://www.ibm.com/cloud/watson-studio

2. Click on _New Project_

3. Select _Data Science_ as type of project.

4. Give a name to your project, and a description for your reference, then setup your project as following and click _Create_.

**Notice 1:** because you are going to share this project with your peer for evaluation, please make sure you have unchecked `Restrict who can be a collaborator`

**Notice 2:** You have to create an IBM Object Storage, if you dont have any IBM Object Storage (you can use the free Lite plan

5. From the top-right, Click on _Add to project_, and then select _Notebook_. 

6. In the _New notebook_ form, click on _From URL_, and enter the Notebook URL: https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-ML0101EN-SkillsNetwork/labs/FinalModule_Courserat/ML0101EN-Proj-Loan-py-v1.ipynb

7. Give the notebook a proper name and description and click on `Create Notebook` to initialize the notebook.

**C. Complete the notebook**

1. Start running the notebook

2. Complete the notebook based on the description in the notebook.

**D. Share the notebook**

1. Click on the share icon on the top-right side of your page

2. Activate the `Share with anyone who has the link`

3. Select `All content excluding sensitive code cells`

4. Copy the link from `Permalink to view notebook`

**How to submit:**

Paste the shared link in the provided text box below, and submit it for peer-review.