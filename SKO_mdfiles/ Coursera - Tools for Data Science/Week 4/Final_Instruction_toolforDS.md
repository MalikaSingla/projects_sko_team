## Peer-graded Assignment: Create and Share Your Jupyter Notebook

### Instructions

Congratulations on finishing all three modules of this course. This week, you'll work on your final assignment which will be graded by your peers.

This course introduced you to multiple data science tools, and in this final project you will use Jupyter Notebook, one of the easiest tools to share publicly.

**Leveraging Jupyter Notebook on IBM Watson Studio, you will create your own Jupyter Notebook (in English) and share it via a public link.**

**Step-By-Step Assignment Instructions**

**How to generate a publicly viewable share link for your Jupyter Notebook:**

**Step 1.** With a Jupyter Notebook open on Watson Studio, first ensure that you have saved the Notebook, by going to "File", then "Save", as shown below.

How to generate a publicly viewable share link for your Jupyter Notebook:

![image](images/Final-1.png)

**Step 2.** Click on the Share button, as shown below:

![image](images/Final-2.png)

**Step 3.** Choose the following settings as shown below, to retrieve your public link to your Jupyter Notebook:

![image](images/Final-3.png)

**Step 4. To ensure that everyone else can view your Notebook, you can visit the link yourself from an incognito or private window.** Alternatively, you can log out of your Watson Studio account and try to visit the link and ensure you can see the content.

![image](images/Final-5.png)