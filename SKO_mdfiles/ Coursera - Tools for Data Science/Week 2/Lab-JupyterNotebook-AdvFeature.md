## Lab - Jupyter Notebook - Advanced Features

**This lab is a continuation of the previous lab.**

**Note:** If you have JupyterLab on Skills Network Labs open in a tab already, continue using that tab. Otherwise, click on "Open Tool" button below to launch JupyterLab.

**Exercise 9 - Advanced Markdown styling**

- You can write HTML code in your Markdown cells. Try executing the following HTML code in a Markdown cell: <a href=https://www.cognitiveclass.ai>Cognitive Class</a>. You should now see a hyperlink to https://www.cognitiveclass.ai that appears as **Cognitive Class**.

- You can also use Markdown formatting. For example, to use an H1 Header, try running the following in a Markdown cell: # Header 1. You can find more rules for Markdown formatting in this Markdown Cheatsheet: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

- Using only Markdown, try creating a table, a list of shopping items, and try embedding an image.

**Passed** 100%

This course uses a third-party tool, Lab - Jupyter Notebook - Advanced Features, to enhance your learning experience. The tool will reference basic information like your name, email, and Coursera ID.

[Open Tool](https://labs.cognitiveclass.ai/tools/jupyterlab/?lti=true)