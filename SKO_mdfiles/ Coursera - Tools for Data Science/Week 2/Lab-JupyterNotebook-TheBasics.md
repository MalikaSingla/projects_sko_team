## Lab - Jupyter Notebook - The Basics

**Special note about the lab environment:**

These Jupyter notebooks labs now use the latest environment: **JupyterLab**, which uses the same .ipynb Jupyter notebook files.

**Exercise 1 - Create a new notebook in Python**

1. If you already have Skills Network Labs open in a tab, you can click on **JupyterLab on the main page.**

2. To create a new notebook, click on any of the languages under "Notebook".

**Exercise 2 - Write and execute code**

1. In your new empty notebook (from Exercise 1), click within the gray code cell and write some code, like "1 + 1" (without quotation marks).

2. Execute the code, by either clicking the Play button in the menu above the notebook, or by pressing Shift+Enter on your notebook.

3. You should see in the output, "2".

4. Try executing other code (try simple math operations).

Great! Now you know how to write and run code in Jupyter Notebooks.

**Exercise 3 - Create new cells**

1. In your Jupyter notebook, first **click on any of the existing cells** to select the cell.

2. From the menu, click on _Insert_, then _Insert Cell Above_ or _Insert Cell Below_.

3. Great! Now you know how to insert new cells in Jupyter Notebooks. Note you can use the keyboard shortcuts: [a] - Insert a Cell Above; [b] - Insert a Cell Below.

**Exercise 4 - Create Markdown cells and add text**

In your notebook, click on any code cell, and in the drop-down menu in the menu above, change the cell type from _Code_ to _Markdown_. As you'll notice, you cannot create Markdown cells without first creating cells and converting them from _Code_ to _Markdown_.

In the Markdown cell, write some text like _My Title_.

To render the Markdown text, make sure the cell is selected (by clicking within it), and press Play in the menu, or Shift+Enter.

Your Markdown cell should now be rendered!

To edit your Markdown cell, double-click anywhere within the cell. Note you can use the keyboard shortcut: [m] - Convert Cell to Markdown

_Please note that practice exercises are not graded, and you do not need submit anything._

**Passed** 100%

This course uses a third-party tool, Lab - Jupyter Notebook - The Basics, to enhance your learning experience. The tool will reference basic information like your name, email, and Coursera ID.






