## Lab: Creating a Watson Studio Project with Jupyter Notebook

This tutorial walks you through setting up an account on the IBM Cloud and a project in Watson Studio such that you can use Jupyter Notebook for your work.

1. Please follow this link to create an [IBM Cloud Account](https://cloud.ibm.com/registration). It's completely for free, you don't need a credit card and the account never expires. 

In case you are getting an error like below, please try with an different email address before you contact support. E.g. a non - gmail address.

![image](images/Lab-3.1.6-regfailure.png)

2. Once you've completed registration and confirmed your email address, please open [dataplatform.cloud.ibm.com](https://dataplatform.cloud.ibm.com/) and click **Log In.**

3. Now please click on **Create a project.**

![image](images/create_project.png)

4. Now please click **Create an empty project.**

![image](images/Lab-3.1.6-createemptyproject1.png)

5. Under **Name**, please type "default", then please click on **Add** under **Define Storage.**

![image](images/Lab-3.1.6-addcos1.png)

6. Once you see the screen below, please scroll down.

![image](images/Lab-3.1.6-addcos2.png)

7. Please make sure the **Lite** plan is selected, then please click **Create.**

![image](images/Lab-3.1.6-addcos3.png)

8. Please click **Confirm.**

9. Please click **Refresh.**

![image](images/Lab-3.1.6-addcos5refresh.png)

10. Please click **Create** to finalize project creation.

![image](images/Lab-3.1.6-fincreateproject.png)

Congratulations, this concludes the first part of the tutorial. Please take a moment to follow through the next steps to learn how you can use Watson Studio Jupyter Notebooks.

1. Please click on **Add to project.**

![image](images/Lab-3.1.6-addtoproject.png)

2. Here you can select an abundance of tools, but let's go for Jupyter Notebooks first. Please click **Notebook.**

![image](images/Lab-3.1.6-addtoprojectnotebook.png)

3. In order to not use up your monthly free compute credits just select the **Default Python 3.6 XS** runtime.

![image](images/DS_2-New-Notebook.png)

4. Please click **Create notebook.**

![image](images/Lab-3.1.6-createnotebook.png)

5. Just wait until the Notebook appears. In case you are interested. The Jupyter enterprise gateway has requested resources on the Kubernetes cluster IBM hosts for serving the Jupyter kernel backing your Notebook.

![image](images/Lab-3.1.6-runtimerampup.png)

6. Now you're ready to code!

![image](images/Lab-3.1.6-testnotebook.png)

This concludes this tutorial.