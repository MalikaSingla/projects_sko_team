## Hands-on LAB: Provision a Cloud Hosted Database Instance

Kindly follow the instructions in the attached file to complete this hands-on lab to create your database instance in Cloud.

[LAB 1 - v6.1 - Create Db2 Instance](./LAB1-v6_1-CreateDb2Instance.md)