## Hands-on LAB 1: Connecting to a database instance (External resource)

This Lab will demonstrate how to create a database connection to an instance of IBM Db2 on Cloud from a Jupyter notebook. You will utilize the Skills Network Labs, a cloud based virtual lab environment, to run the JupyterLab tool. When you click on the "Launch JupyterLab in New Tab" button below you will be asked to confirm sharing your username and email. Its usage will be according to the Privacy Policy linked in the tool. In case your browser security blocks new browser windows/pop-ups, please choose the option to always allow from our site.

[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/DB0201EN/DB0201EN-Week3-1-1-Connecting-v4-py.ipynb?lti=true)


In case, you encounter any issue in launching  Skills Network Labs or want to view the notebook in your own Jupyter environment, you may download the Jupyter notebook by right clicking on the link below and choosing  "Save Link As..."

[Download lab here](https://ibm.box.com/shared/static/e2v1r94o3u37vda7fxxnzxzqowc5uirn.ipynb)