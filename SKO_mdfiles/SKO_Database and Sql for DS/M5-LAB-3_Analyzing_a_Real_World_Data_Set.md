## Hands-on LAB 3: Analyzing a Real World Data Set

In this Lab you will utilize a real world data set and analyze it using SQL queries. As in the previous Lab, you will access a Db2 on Cloud database from JupyterLab using the simplified SQL Magic interface. You will utilize the Skills Network Labs, a cloud based virtual lab environment, to run the JupyterLab tool. When you click on the "Launch JupyterLab in New Tab" button below you will be asked to confirm sharing your username and email. Its usage will be according to the Privacy Policy linked in the tool. In case your browser security blocks new browser windows/pop-ups, please choose the option to always allow from our site.

[Launch Jupyter Lab in New Tab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/DB0201EN/DB0201EN-Week3-1-4-Analyzing-v5-py.ipynb?lti=true)

In case, you encounter any issue in launching  Skills Network Labs or want to view the notebook in your own Jupyter environment, you may download the Jupyter notebook by right clicking on the link below and choosing  "Save Link As..."

[Download the lab here](https://ibm.box.com/shared/static/slhr5tl3gmrwzzymu2ivld35ktfc8fn1.ipynb)