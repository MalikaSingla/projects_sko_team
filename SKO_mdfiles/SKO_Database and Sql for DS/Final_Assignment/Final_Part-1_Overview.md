## Overview

_Scenario_

The homelessness situation in a large urban area like New York can be immense. In an effort to re-market the possible solutions and develop strategies a plan has been drawn up to examine the gap in the jobs available, the working population and the homeless in order to see what kind of and how large the gaps and difficulties are. You have been tasked to examine this matter and have been given the following datasets from the NYC Open Data portal:

- [https://data.cityofnewyork.us/City-Government/NYC-Jobs/kpav-sd4t](https://data.cityofnewyork.us/City-Government/NYC-Jobs/kpav-sd4t)

- [https://data.cityofnewyork.us/City-Government/Citywide-Payroll-Data-Fiscal-Year-/k397-673e](https://data.cityofnewyork.us/City-Government/Citywide-Payroll-Data-Fiscal-Year-/k397-673e)

- [https://data.cityofnewyork.us/Social-Services/Directory-Of-Homeless-Population-By-Year/5t4n-d72c](https://data.cityofnewyork.us/Social-Services/Directory-Of-Homeless-Population-By-Year/5t4n-d72c)

A Jupyter notebook has been provided to help with completing this assignment. Follow the instructions to complete all the problems. Then share the Queries and Results with your peers for reviewing.

_Review Criteria_

The total points possible for this assignment are 25. Here is the breakdown:

- Problem 1: 1 Points

- Problem 2: 1 Points

- Problem 3: 2 Points

- Problem 4: 2 Points

- Problem 5: 2 Points

- Problem 6: 2 Points

- Problem 7: 2 Points

- Problem 8: 4 Points

- Problem 9: 4 Points

- Problem 10: 5 Points

For each problem points will be awarded as follows:

- Full points: Used a correct SQL query that yielded a correct result

- Half or partial points: The query and results are not fully correct

- No points: Did not attempt the problem or did not upload any solution

This assignment uses a third-party tool, Jupyter notebook with problems for Peer Reviewed Assignment, to enhance your learning experience. The tool will reference basic information like your name, email, and login ID.

_About the Datasets_

To complete the assignment problems in this notebook you will be using three datasets that are available on the NYC Open Data Portal:

- [New York City Jobs](https://data.cityofnewyork.us/City-Government/NYC-Jobs/kpav-sd4t)

- [Directory of Homelessness Population by Year](https://data.cityofnewyork.us/Social-Services/Directory-Of-Homeless-Population-By-Year/5t4n-d72c)

- [Citywide Payroll Data](https://data.cityofnewyork.us/City-Government/Citywide-Payroll-Data-Fiscal-Year-/k397-673e)

**1. New York City Jobs**

This dataset contains a selection of job postings available on the City of New York’s official jobs site. Internal postings available to city employees and external postings available to the general public are included.

For this assignment you will use a snapshot of this dataset which can be downloaded from: https://www.philosophynews.com/data/NYC_Jobs.csv

A detailed description of this dataset and the original dataset can be obtained from the NYC Open Data Portal at: https://data.cityofnewyork.us/City-Government/NYC-Jobs/kpav-sd4t

**2. Directory of Homelessness by Year**

This dataset reflects reported homeless population by year.

For this assignment you will use a snapshot of this dataset which can be downloaded from: http://www.philosophynews.com/data/Directory_Of_Homeless_Population_By_Year.csv

A detailed description of this dataset and the original dataset can be obtained from the NYC Open Data Portal at: https://data.cityofnewyork.us/Social-Services/Directory-Of-Homeless-Population-By-Year/5t4n-d72c

**3. Citywide Payroll Data**

This dataset reflects all government employees, their positions, and their salaries over a number of years.

The original dataset is around 1GB in size and contains over 3.3 million rows. For the purposes of this assignment you will use a much smaller sample with only about 1000 rows, selected randomly. It can be downloaded from: http://www.philosophynews.com/data/Citywide_Payroll_Data_Sample.csv

A detailed description of this dataset and the original dataset can be obtained from the NYC Open Data Portal at: https://data.cityofnewyork.us/City-Government/Citywide-Payroll-Data-Fiscal-Year-/k397-673e

**Download the datasets**

In many cases the dataset to be analyzed is available as a .CSV (comma separated values) file, perhaps on the internet. Click on the links below to download and save the datasets (.CSV files):

**1. NYC_JOBS:** https://www.philosophynews.com/data/NYC_Jobs.csv

**2. HOMELESS_POPULATION:** http://www.philosophynews.com/data/Directory_Of_Homeless_Population_By_Year.csv

**3. CITY_PAYROLL:** http://www.philosophynews.com/data/Citywide_Payroll_Data_Sample.csv

**NOTE:** Ensure you have downloaded the datasets using the links above instead of directly from the NYC Open Data Portal. The versions linked here are subsets of the original datasets and have some of the column names modified to be more database friendly which will make it easier to complete this assignment.

**Store the datasets in database tables**

To analyze the data using SQL, it first needs to be stored in the database.

While it is easier to read the dataset into a Pandas dataframe and then PERSIST it into the database as we saw in Week 3 Lab 3, it results in mapping to default datatypes which may not be optimal for SQL querying. For example a long textual field may map to a CLOB instead of a VARCHAR.

Therefore, **it is highly recommended to manually load the table using the database console LOAD tool, as indicated in Week 2 Lab 1 Part II**. The only difference with that lab is that in Step 5 of the instructions you will need to click on create "(+) New Table" and specify the name of the table you want to create and then click **Next**.

![image](images/DB2_on_Cloud.jpg)

Now open the Db2 console, open the LOAD tool, Select / Drag the .CSV file for the first dataset, click Next to create a New Table, and then follow the steps on-screen instructions to load the data. Name the new tables as follows:

- NYC_JOBS

- HOMELESS_POPULATION

- CITY_PAYROLL 

**Connect to the database**

Let us first load the SQL extension and establish a connection with the database.

%load_ext sql

In the next cell enter your db2 connection string. Recall you created Service Credentials for your Db2 instance in first lab in Week 3. From the uri field of your Db2 service credentials copy everything after db2:// (except the double quote at the end) and paste it in the cell below after ibm_db_sa://

![image](images/Db2-fk.jpg)

```sql
# Remember the connection string is of the format:

# %sql ibm_db_sa://my-username:my-password@my-hostname:my-port/my-db-name

# Enter the connection string for your Db2 on Cloud database instance below

%sql ibm_db_sa://

```

## Author(s)
<h4> Rav Ahuja <h4/>


## Change log
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-09-05 | 2.0 | Malika Singla | Migrated Lab to Markdown |



## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
