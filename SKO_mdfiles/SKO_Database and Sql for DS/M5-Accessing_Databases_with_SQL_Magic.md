## Hands-on Tutorial: Accessing Databases with SQL Magic

In this hands-on tutorial you will create a table, insert some data, and retrieve the results using SQL magic. NOTE: In the previous Lab you used Jupyter notebooks. In this Lab you will utilize JupyterLab, the next generation UI for Project Jupyter. Additionally, you will utilize SQL magic within JupyterLab, to simplify issuing your queries and seeing their results. You will utilize the Skills Network Labs, a cloud based virtual lab environment, to run the JupyterLab tool. When you click on the "Launch JupyterLab in New Tab" button below you will be asked to confirm sharing your username and email. Its usage will be according to the Privacy Policy linked in the tool. In case your browser security blocks new browser windows/pop-ups, please choose the option to always allow from our site.

[Launch JupyterLab in New Tab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/DB0201EN/DB0201EN-Week3-1-3-SQLmagic-v3-py.ipynb?lti=true)

In case, you encounter any issue in launching  Skills Network Labs or want to view the notebook in your own Jupyter environment, you may download the Jupyter notebook by right clicking on the link below and choosing  "Save Link As..."

[Download the Lab from here](https://ibm.box.com/shared/static/3tjcyg2a5tidz0wer3vvh2a3398zh8h2.ipynb)