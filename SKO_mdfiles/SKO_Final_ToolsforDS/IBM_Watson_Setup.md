## IBM Watson Setup

_If you have not created a Watson service before proceed with Step 1, otherwise go to Step 2:_

### Step 1: For New Users (with no Watson service):

For this project, you will use your IBM Watson Studio account from the previous chapter.  

Go to the IBM Cloud Watson Studio page:

[Click here](https://cloud.ibm.com/login)

You will see the screen in the figure below. Click the icon in the red box:

![image](images/F1_Create.png)

Then click **Watson**, as shown below:

![image](images/F2_Create.png)

Then click **Browse Services.**

![image](images/F3_Browse.png)

Scroll down and select **Watson Studio - Lite.**

![image](images/F4_SelectWatson.png)

To create a Watson service using the Lite plan, click **Create.**

![image](images/F5_CreateService.png)

Now click **Get Started.**

![image](images/F6_GetStarted.png)

After creating the service continue with **Step 2.**

### Step 2: For Existing Users (who already have Watson Service):

Go to the IBM Cloud Dashboard and click **Services.**

![image](images/F7_Service.png)

When you click on Services, all your existing services will be shown in the list. Click the **Watson Studio** service you created:

![image](images/F8_Selectwatson.png)

Then click **Get Started.**

![image](images/F6_GetStarted.png)

**Step 3: Creating a Project**

Now you have to Create a project.

Click on **Create a project:**

![image](images/F9_Create_project.png)

On the Create a project page, click **Create an empty project**

![image](images/F9_EmptyProject.png)

Provide a **Project Name** and **Description**, as shown below:

![image](images/F10_ProjectDetails.png)

You must also create storage for the project.

Click **Add**

![image](images/F11_AddStorage.png)

On the Cloud Object Storage page, scroll down and then click **Create.**

![image](images/F12_StorageLite.png)

In the Confirm Creation box, click **Confirm.**

![image](images/F13_Confirm.png)

On the New project page, note that the storage has been added, and then click **Create.**

![image](images/F14_AddProject.png)

After creating the project continue with Step 3.

### Step 3: Adding a Notebook to the Project:

You need to add a Notebook to your project. Click **Add to project.**

![image](images/F15_ProjectCreated.png)

In the list of asset types, click **Notebook:**

![image](images/F16_Notebook.png)

Note: Python is the default selected language. You may choose a different language, such as R or Scala, from the Select Runtime drop down menu options.


On the New Notebook page, enter a name for the notebook, and then click **Create Notebook.**

![image](images/F17_CreateNotebook.png)

After you complete your notebook you will have to share it. Select the icon on the top right marked in red in the image below, a dialogue box should open, select the option all content excluding sensitive code cells.

![image](images/F18_Share.png)

Copy the link to the Notebook and share it.

![image](images/F19_Share2.png)

![image](images/F20_Share3.png)



