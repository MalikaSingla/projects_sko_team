## Peer-graded Assignment - Create and Share Your Jupyter Notebook

**Welcome to the final assignment!**

Great! You have now completed all three modules of this course. This week, you will complete the final assignment that will be graded by your peers. In this assignment, you will create markdown in a Jupyter notebook using IBM Watson Studio.

**Instructions:**

Create a Jupyter Notebook using IBM Watson Studio. You can choose any language you want (Python, R, or Scala). You can leverage the [Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) to help with the correct syntax to style your markdown.

Guidelines for the submission:

Create your Jupyter Notebook on [IBM Watson Studio](https://cloud.ibm.com/catalog/services/watson-studio).

To learn how to create a Notebook on IBM Watson Studio, follow the steps in this [guide](./IBM_Watson_Setup.md).

Include at least 6 cells with the following criteria:

- **Cell 1 (rendered as Markdown)**: Using markdown, enter a title with H1 header style that says **Jupyter Notebook in IBM Watson Studio**. The title may or may not be centered.

- **Cell 2 (rendered as Markdown)**: Include your name, in bold characters. In the line below your name, write **Data Scientist in Training** as your occupation in regular font.

- **Cell 3 (rendered as Markdown)**: In regular font, explain what the code in Cell 4 will do and what will be the output. The code should be simple and could even be a simple arithmetic operation (such as **2 * 2 = 4**).  

- **Cell 4 (rendered as Markdown)**: Write the code you described in Cell 3 and execute it.

- **Cell 5**: Using Markdown or HTML, this cell must include at least 3 of the following: tables, horizontal rule, bulleted list, hyperlinks, code/syntax highlighting, images, blocked quotes, strikethrough, or a numbered list.

- **Cell 6 (rendered as Markdown)**: In Calibri font, text size 11, write at least two sentences stating why you want to be a data scientist.