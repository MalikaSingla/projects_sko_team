This is the instruction to create an instance of IBM Speech to Text, and IBM Language Translator. IBM Speech to Text is a service that converts speech in audio format into text. Watson Speech to Text is a service that uses deep learning algorithms to convert speech to text. IBM Language Translator converts one language to another.

NOTE: In order to complete this the lab in this section you will be creating an IBM Cloud account and create an instance of IBM Speech to Text and IBM Language Translator and obtain their respective API Keys. A credit card is NOT required to sign up for IBM Cloud Lite account and there is no charge associated in creating a Lite plan instance of the Watson VR service.

### Step 1: Create an IBM Cloud Account

Click on the link below to create an IBM cloud account:

[Sign Up for IBM Cloud](https://cloud.ibm.com/catalog/services/watson-studio)

On the page to which you get redirected by clicking on the link above, enter your email address, first name, last name, country or region, and set your password.

**NOTE:** To get enhanced benefits, please sign up with you company email address rather than a free email ID like Gmail, Hotmail, etc.

If you would like IBM to contact you for any changes to services or new offerings, then check the box to accept the box to get notified by email. Then click on the Create Account button to create your IBM Cloud account. **If you already have an IBM Cloud account you can just log in using the link above the Email field (top right in the screenshot below).

![image](images/Sign_Up.png)

### Step 2: Confirm Your Email Address

An email is sent to your email address to confirm your account.

![image](images/Email_Confirmation.png)

Go to your email account, and click on the **Confirm Account** link in the email that was sent to you.

![image](images/Confirm_Email_Account.png)

### Step 3: Login to Your Account

![image](images/Log_into_account.png)

When you click [Log in](https://cloud.ibm.com/login), you will be redirected to a page to log into your IBM Cloud account.

![image](images/loginto_account.png)

### Step 4: Create a Speech to Text Instance

To create a speech to text instance click [Speech to Text](https://cloud.ibm.com/catalog/services/speech-to-text?cm_mmc=Email_Newsletter-_-Developer_Ed%2BTech-_-WW_WW-_-SkillsNetwork-Courses-IBMDeveloperSkillsNetwork-PY0101EN-SkillsNetwork-19487395&cm_mmca1=000026UJ&cm_mmca2=10006555&cm_mmca3=M12345678&cvosrc=email.Newsletter.M12345678&cvo_campaign=000026UJ)


On the next page, you will get to name your service instance and choose your region. Click on the arrow to reveal the drop-down menu of regions. Make sure to select the region that is closest to you. Since I am located in Canada, then I am choosing Dallas as my region since it is the closest region to me.

![image](images/Naming_Speech_to_Text_Service.png)

Then scroll down and make sure that the **lite** plan is selected, and click the **Create** button.

![image](images/Create_Speech_to_Text.png)

### Step 5: Save your Speech to Text API Key

Go to **Manage**, then save your Speech to Text **API Key**, and **URL** you will need it for the labs in this section.

![image](images/Speech_to_Text_API.png)

### Step 7: Create a Language Translator Instance

To create a translation instance click [Language Translator](https://cloud.ibm.com/catalog/services/language-translator?cm_mmc=Email_Newsletter-_-Developer_Ed%2BTech-_-WW_WW-_-SkillsNetwork-Courses-IBMDeveloperSkillsNetwork-PY0101EN-SkillsNetwork-19487395&cm_mmca1=000026UJ&cm_mmca2=10006555&cm_mmca3=M12345678&cvosrc=email.Newsletter.M12345678&cvo_campaign=000026UJ)

On the next page, you will get to name your service instance and choose your region. Click on the arrow to reveal the drop-down menu of regions. Make sure to select the region that is closest to you. Since I am located in Canada, then I am choosing Dallas as my region since it is the closest region to me.

![image](images/Naming_Language_Translator.png)

Then scroll down and make sure that the **lite** plan is selected, and click the **Create** button.

![image](images/Create_Translation.png)

### Step 5: Save your Speech to Text API Key

Go to **Manage**, then save your Speech to Text **API Key**, and **URL** you will need it for the labs in this section.

![image](images/Translation_API.png)

## Author(s)
<h4> Joseph Santarcangelo <h4/>


## Change log
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-09-05 | 2.1 | Malika Singla | Updated the screenshot |
| 2020-08-25 | 2.0 | Srishti | Migrated Lab to Markdown |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
