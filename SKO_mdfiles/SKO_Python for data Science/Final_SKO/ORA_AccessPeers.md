## Assess peer responses

In the peer assessment step of an [open response assessment](https://gitlab.com/MalikaSingla/projects_sko_team/-/blob/master/SKO_mdfiles/Python%20for%20data%20Science/finalexam_ORA.md), you perform assessments of responses that were submitted by other learners in your course. The course team sets the requirement for how many peer assessments each learner is expected to complete.

## How to Assess Peer Responses
At the top of the peer assessment step, you can see counts of how many responses you are expected to assess and how many you have already assessed. For example, if you are required to perform 3 peer assessments and are about to start your first peer assessment, the count appears as “1 of 3”.

**Note:** If there are no submitted responses available for grading, a status message indicates that no peer responses are currently available for you to assess, and that you should check back later.

Within the Assess Peers step, you see each question, a learner’s response, and the rubric that you will use to grade the response.

You assess other learners’ responses by selecting options in the rubric. This process will be familiar to you if your assignment included the learn to assess responses step. Additionally, this step has a field below the rubric where you can provide comments about the learner’s response.

`Note: In addition to a field for overall comments on a learner’s response, some peer assessments include Comments fields for individual criteria that allow you to enter up to 300 characters. In some assessments, you must enter comments before you can submit the assessment.`

After you have selected options in the rubric and provided comments about the response, select Submit your assessment and move to response #{number}.

After you submit each peer assessment, a response from another learner becomes available, until you have assessed the required number of responses. The count of how many responses you have assessed updates after you assess each response.

When you have completed the required number of peer assessments, the next step in the assignment becomes available.

## Optional: Assess Additional Peer Responses
If you have assessed the required number of peer responses, the peer assessment step collapses so that only the Assess Peers heading is visible.

If you want to, you can assess more peer responses than the assignment requires. To assess more responses, select the Assess Peers heading to expand the step, and then select Continue Assessing Peers.