## View your staff grade

In some [open response assessments](https://gitlab.com/MalikaSingla/projects_sko_team/-/blob/master/SKO_mdfiles/Python%20for%20data%20Science/finalexam_ORA.md), a staff assessment step is included for a member of the course team to grade your responses. You do not need to take any action for this step. The status of the Staff Grade step changes to Complete when a member of the course team has completed grading your response.

If a Staff Grade step exists in your assignment, you receive your final assignment grade when staff grading is complete, even if your response has not been assessed by the required number of peer reviewers.

`Note: Course staff can grade your open response assignment even if a staff assessment step is not included in the assignment. This might happen if, for example, you receive peer assessments of your response that are inappropriate. In such cases, course staff can perform an assessment of your response that overrides any peer assessment grades. If a member of the course staff has graded your response, a Staff Grade section appears in the grading details for your assignment.`

