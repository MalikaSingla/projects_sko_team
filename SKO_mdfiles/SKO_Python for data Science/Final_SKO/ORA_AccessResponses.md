## Learn to assess responses

Some [open response assessment](https://gitlab.com/MalikaSingla/projects_sko_team/-/blob/master/SKO_mdfiles/Python%20for%20data%20Science/finalexam_ORA.md) (ORA) assignments include a training step, so that you can learn how to effectively assess responses for a later peer assessment step.

In a training step, you evaluate example responses using a provided rubric as a guide for grading. After you complete the grading, you are shown how the grades you gave differ from the grades that a member of the course team gave. The goal is to learn how to assess responses similar to the way that course staff would assess them, using the same rubric.

## To Learn How to Assess Responses:
1. Read each sample response and the rubric carefully, then for each criterion, select the option that you think best reflect the response.

2. When you are satisfied with your assessment, select Compare your selections with the instructor’s selections.

    - If all of your selections are the same as the instructor’s selections, the next sample response opens automatically.

    - If any option that you select is not the same as the instructor’s selection, you see the response again, with a message indicating that your assessment differs from the instructor’s assessment.

3. If your assessment did not match the instructor’s assessment, review the response again and consider why the instructor assessed the response differently than you did. Continue to assess the example response until the options you select for all criteria match options selected by the instructor.

When you have successfully assessed the sample responses, the next step in the assignment becomes available.