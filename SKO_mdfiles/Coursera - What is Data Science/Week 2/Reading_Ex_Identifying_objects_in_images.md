## Exercise: Identifying objects in images with IBM Watson

### About this exercise:

In this lesson, we'd like to take you on a bit of a side journey. It's a fun exercise and we hope you enjoy it as much as we enjoyed putting this short exercise together for you. This exercise is ungraded, so feel free to have fun with it!

### Image Classification with IBM Watson Visual Recognition:

What better way to understand the applications of data science than to try it out yourself? You'll be uploading images and seeing how IBM Watson identifies the various objects and faces (even gender and age!) in your images.

![image](images/image1.png)


To learn how to classify your own images, continue on to the next reading!

