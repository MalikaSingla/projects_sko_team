## Exercise: Upload and classify your images!

### Lab overview:

#### Scenario
IBM Watson Visual Recognition (VR) is a service that uses deep learning algorithms to identify objects and other content in an image. In this hands-on lab, you will use Watson VR to upload and classify images. 

**Note:** To complete this exercise, you will create an IBM Cloud account and provision an instance of the Watson Visual Recognition service. A credit card is NOT required to sign up for an IBM Cloud Lite account and there is no charge associated in creating a Lite plan instance of the Watson VR service.

#### Objectives

After completing this lab, you will be able to:

1. Access IBM Cloud

2. Add resources to your IBM Cloud account

3. Add services to your IBM Cloud account

4. Create a project in Watson Studio

5. Analyze images using Watson VR

### Exercise 1: Create an IBM Cloud Account

#### Scenario

To access the resources and services that the IBM Cloud provides, you need an IBM Cloud account. 

If you already have an IBM Cloud account, you can skip Tasks 1 and 2 and proceed with Task 3: Login to you IBM Cloud account.

#### Task 1: Sign up for IBM Cloud

1. Go to: [Create a free account on IBM Cloud](https://cocl.us/ibm_watson_visual_recognition_ai101_coursera)

2. In the Email box, enter your email address and then click the arrow.

![image](images/01_Sign_up_page1.png)

3. When your email address is accepted, enter your **First Name, Last Name, Country or Region,** and create a **Password**.

**Note**: To get enhanced benefits, please sign up with your company email address rather than a free email ID like Gmail, Hotmail, etc.

If you would like IBM to contact you for any changes to services or new offerings, then check the box to accept the option to be notified by email.

Then click **Create Account** to create your IBM Cloud account.

#### Task 2: Confirm your email address

1. An email is sent to the address that you signed up with.

![image](images/02_Sign_up_message1.png)

2. Check your email, and in the email that was sent to you, click **Confirm Account.**

![image](images/03_Confirm_Account.png)

3. You will receive notification that your account is confirmed.

![image](images/04_Welcome_msg1.png)

Click **Log In**, and you will be directed to the IBM Cloud Login Page.

#### Task 3: Login to your IBM Cloud account

1. On the [Log in to IBM Cloud page](https://cloud.ibm.com/login), in the ID box, enter your email address and then click **Continue.**

![image](images/05_Login_1.png)

2. In the **Password** box, enter your password, and then click **Log in.**

![image](images/06_Login_2.png)

### Exercise 2: Create a Watson Studio Resource

#### Scenario

To manage all your projects, you will use IBM Watson Studio. In this exercise, you will add Watson Studio as a Resource.

#### Task 1: Add Watson Studio as a resource

1. On the Dashboard, click Create Resource.

![image](images/07_Create_Resource_1.png)

2. In the Catalog, click **AI (16).**

![image](images/08_Catalog.png)

Note that the **Lite** Pricing Plan is selected.

3. In the list of **Services**, click **Watson Studio.**

![image](images/09_Watson_Studio_Catalog.png)

4. On the Watson Studio page, select the region closest to you, verify that the **Lite** plan is selected, and then click **Create.**

![image](images/10_Watson_Studio_Lite_sign_up_2.png)

5. When the Watson Studio resource is successfully created, you will see the Watson Studio page. Click **Get Started.**

![image](images/11_Watson_Studio2.png)

6. You will see this message when Watson Studio is successfully set up for you. 

![image](images/12_Watson_apps_ready.png)

Click **Get Started.**

## Exercise 3: Create a project

#### Scenario

To manage all the resources and services that you are working with, you should create a Watson Studio Project. You will begin by creating an empty project, and then adding the resources and services that you need. 

#### Task 1: Create an empty project

1.On the Watson Studio Welcome page, click **Create a project.**

![image](images/13_create_project.png)

2. On the Create a project page, click **Create an empty project**

![image](images/14_Create_project_5.png)

3. On the New project page, enter a **Name** and **Description** for your project.

![image](images/15_New_Project_details_2.png)

4. You must define storage for your project before you can create it. Under **Select storage service**, click **Add**.

![image](images/16_New_Project_details_3.png)

5. On the Cloud Object Storage page, verify that **Lite** is selected, and then click **Create.**

![image](images/17_Select_storage_2.png)

6. In the Confirm Creation box, click **Confirm.**

![image](images/18_Confirm_Creation_2.png)

7. On the New project page, under **Define storage**, click **Refresh**, and then click **Create.**

## Exercise 4: Add a Watson VR Service instance

#### Scenario

This project will focus on analyzing images, so you need to add the Watson Visual Recognition Service. You will also need some images to analyze, so follow the setup steps below to ensure you are prepared.

#### Setup 

Before you begin this exercise, you must complete the following steps:

1. Collect a set of at least 20 images. You can use your own images, or download them from the internet. 

2. Store the images in an easy to find location.

#### Task 1: Add the Visual Recognition Service

1. To add services to the project, click **Add to project.**

![image](images/19_Add_to_Project_3.png)

2. In the Choose asset type box, click **Visual Recognition.**

![image](images/20_Add_VR.png)

3. In the Associate a service box, click **here.**

![image](images/21_Associate_Service.png)

4. On the Visual Recognition page, verify that Lite is selected, and then click **Create.**

![image](images/22_Visual_Recognition_Create.png)

5. In the Confirm Creation box, click **Confirm.**

![image](images/23_VR_Confirm_Creation.png)

#### Task 2: Analyze images with Watson VR

1. Now you can see all the built-in image classification models that IBM Watson provides! Let's try the General model.

To analyze your images, on the Models page, under **Prebuilt Models**, in the General box, click **Test.**

![image](images/24_VR_Models.png)

2. On the General page, click the **Test** tab.

![image](images/25_General_page_Test.png)

3. To upload images, on the Test tab, click **Browse.**

![image](images/26_Test_Browse_for_images.png)

4. Select the images you want to upload and then click **Open.**

![image](images/27_Select_images.png)

5. Once you have uploaded your images, Watson Studio Visual Recognition will tell you what it thinks it found in your images! Beside each class of object (or color, age, etc.), it gives you a confidence score (between 0 and 1) showing how confident it is that it found that particular object or feature in your image (0 for lowest confidence and 1 for highest confidence).

![image](images/28_Uploaded_images.png)

6. Use the check boxes on the left to filter the images. In this example, only images in which Watson VR has detected beige color are displayed.

![image](images/29_beige_images_only.png)

7. Use the Threshold slider to only display images in which Watson VR has at least 90% confidence of the beige color. 

![image](images/30_High_confidence_in_beige.png)

#### Task 3: Share your results

Follow us on Twitter and send us some of the funniest and most interesting results you found with IBM Watson Visual Recognition!

![image](images/Polong-Lin-Tweet-sample-2.png)

[Follow Polong Lin](https://twitter.com/polonglin)

[Follow Alex Aklson](https://twitter.com/aklson_DS)

