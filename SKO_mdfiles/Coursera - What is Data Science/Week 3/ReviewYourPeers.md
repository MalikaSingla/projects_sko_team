## Peer-graded Assignment: Final Assignment

### Ready to review your classmates?

Reviewing another person's work is a valuable way to learn and providing quality feedback is a useful skill to master. Here are some tips to help you provide the most helpful, accurate, and consistent feedback possible:

- Remember, English isn't everyone's native language. Focus primarily on how well the work meets the grading criteria.

- When writing critical feedback, use a constructive and polite tone.

Everyone enrolled in the course must review at least 3 other submissions to ensure everyone receives a grade; however, many learners complete more to help their classmates who are still waiting. Ready to get started?

[Start Reviewing](https://www.coursera.org/learn/what-is-datascience/peer/dgsjq/final-assignment/review-next)