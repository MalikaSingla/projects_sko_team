**Course Text Book: ‘Getting Started with Data Science’ Publisher: IBM Press; 1 edition (Dec 13 2015) Print.**

**Author: Murtaza Haider**

![image](images/Book.PNG)

Prescribed Reading: Chapter 3 Pg. 60-62

![image](images/book_page60.png)

![image](images/book_page62.png)
