## Lesson Summary

In this lesson, you have learned:

- The importance of stakeholder input.

- To consider the scale of deployment.

- The importance of incorporating feedback to refine the model.

- The refined model must be redeployed.

- This process should be repeated as often as necessary.