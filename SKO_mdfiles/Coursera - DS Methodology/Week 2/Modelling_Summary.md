## Lesson Summary

In this lesson, you have learned:

- The difference between descriptive and predictive models.

- The role of training sets and test sets.

- The importance of asking if the question has been answered.

- Why diagnostic measures tools are needed.

- The purpose of statistical significance tests.

- That modeling and evaluation are iterative processes.