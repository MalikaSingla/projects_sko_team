## Lesson Summary

In this lesson, you have learned:

- The importance of descriptive statistics.

- How to manage missing, invalid, or misleading data.

- The need to clean data and sometimes transform it.

- The consequences of bad data for the model.

- Data understanding is iterative; you learn more about your data the more you study it.