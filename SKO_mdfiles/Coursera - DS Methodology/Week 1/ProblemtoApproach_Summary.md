## Lesson Summary

In this lesson, you have learned:

- The need to understand and prioritize the business goal.

- The way stakeholder support influences a project.

- The importance of selecting the right model.

- When to use a predictive, descriptive, or classification model.