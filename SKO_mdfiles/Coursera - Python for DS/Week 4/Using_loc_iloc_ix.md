## Using loc, iloc and ix

There are three ways to select data from a data frame in Pandas: _loc, iloc, and ix._

### loc

_loc_ is primarily label based; when two arguments are used, you use column headers and row indexes to select the data you want. _loc_ can also take an integer as a row or column number.

Examples of _loc_ usage:

![image](images/1_loc.png)
_loc_ will return a _KeyError_ if the requested items are not found.

### iloc 

_iloc_ is integer-based. You use column numbers and row numbers to get rows or columns at particular positions in the data frame. 

Examples of _iloc_ usage:

![image](images/2_loc.PNG)

_iloc_ will return an _IndexError_ if the requested indexer is out-of-bounds.

### ix
By default, _ix_ looks for a label. If ix doesn't find a label, it will use an integer. This means you can select data by using either column numbers and row numbers or column headers and row names using _ix_.

In Pandas version 0.20.0 and later, _ix_ is deprecated.

### Using loc and iloc for slicing
You can also use _loc_ and _iloc_ to slice data frames and assign the values to a new data frame. 

#### Creating a new dataframe with loc slicing
You can also slice data frames and assign the values to a new data frame using the column names. The code assigns the first three rows and all columns in between to the columns named Artist and Released. The result is a new data frame Z with the corresponding values.

![image](images/3_loc.png)

#### Creating a new dataframe with iloc slicing

In this example, we assign the first two rows and the first three columns to the variable Z. The result is a data frame comprised of the selected rows and columns. 

![image](images/4_loc.png)


## Author(s)
<h4> Joseph Santarcangelo <h4/>


## Change log
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-09-05 | 2.1 | Malika Singla | Updated the screenshot |
| 2020-08-25 | 2.0 | Lavanya | Migrated Lab to Markdown and added to course repo in GitLab |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>