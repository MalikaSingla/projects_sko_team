## Steps in an Open Response Assessment (ORA) assignment

Open response assessments can have several possible steps, which appear in the order that you must complete them. Future steps are not available until you complete your current step. This topic describes all of the possible steps. The actual steps in your assignment depend on how your course team has designed the assignment.

- Your Response: In this first step in an open response assessment, you submit your response to the assignment question.
Learn to Assess Responses: In this step, you practice grading some responses. You evaluate example responses and then see how the grade you gave differs from the grade that a member of the course team gave. The goal is to learn how to assess responses similar to the way that course staff would assess them, using the same rubric.
- Assess Peers: You grade responses that other learners in the course have submitted, and other learners in the course grade your responses.
- Assess Your Response: In this step, you assess your own response, using the same rubric that you used to perform peer assessments.
- Staff Grade: Members of the course team assess your response. If you receive a staff grade for your assignment, it always overrides any peer assessment grades that you receive.